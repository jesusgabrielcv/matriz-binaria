/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author gabriel
 */
public interface OperacionMatriz {
    
    /*Retorna un vector de int con el decimal que representa cada fila, genera una excepcion si la matriz no contiene elementos
    */
    public int[]getVectorDecimal()throws Exception;
    
    /*Retorna true si la matriz binaria es dispersa, genera una excepción si la matriz no contiene elementos
    */
    public boolean isDispersa()throws Exception;
}
