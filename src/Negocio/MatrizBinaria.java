/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author gabriel
 */
public class MatrizBinaria implements OperacionMatriz {
    private boolean m[][];
    
    public MatrizBinaria(){}
    
    public MatrizBinaria(String datos)throws Exception{
        if (datos.isEmpty())throw new Exception("la cadena esta vacia");
        String x[]=datos.split(";");
        this.m= new boolean[x.length][];
        for (int i = 0; i < x.length; i++) {
            String z[]=x[i].split(",");
            this.m[i]= new boolean[z.length];
            for (int j = 0; j < z.length; j++) {
                if (z[j].equals("0")||z[j].equals("1")){
                boolean xy;
                    if (z[j].equals("1"))xy=true;
                    else xy=false;
                    
                    m[i][j]=xy;}
                else{
                throw new Exception("la cadena contiene numeros diferentes de 0 y 1");
                }
            }
            
        }
    }
    
    
    public int getDecimal(boolean[] o){
        int op,sum=0;
        for (int i = 0; i < o.length; i++){
            if (o[o.length-i-1]) 
                 sum+=(int)Math.pow(2, i);
            
           
        }
        return sum;
    }
    
    
    @Override
    public int[] getVectorDecimal() throws Exception {
       int v[]=new int [m.length];
        for (int i = 0; i < v.length; i++) 
            v[i]=getDecimal(m[i]);
            
        return v;
    }

    
    @Override
    public boolean isDispersa()throws Exception{
    
        if(this.m==null)throw new Exception("la matriz no contiene elementos");
        for (int i = 0; i < m.length-1; i++) {
            if(m[i].length!=m[i+1].length)return true;
            
        }
        
    return false;
    }

    
    public MatrizBinaria getSumar(MatrizBinaria m2)throws Exception{
        if (this.m==null || m2.m==null) throw new Exception("cadenas vacias");
        MatrizBinaria m3=m2;
        if(m2.m.length!=this.m.length)throw new Exception("no cumple primer requisito para sumar: filas desiguales");
        for (int i = 0; i < this.m.length; i++) {
            if (this.m[i].length!=m2.m[i].length) throw new Exception("no cumple segundo requisito para sumar: columnas desiguales");
            int sum=0;
            sum=getDecimal(this.m[i])+getDecimal(m2.m[i]);
            
            m3.m[i]=getBinarioVector(getBinarioString(sum));
            
        }
    
    return m3;
    }
    
    private String getBinarioString(int dato){
        String msg="";
        int op;
        while (dato>0) {            
            op=dato%2;
            if (op==1) msg+=true+",";
            else msg+=false+",";
           
            dato/=2;
        }
    return msg;
    }
    
    private boolean[] getBinarioVector(String x){
    String v[]=x.split(",");
            
    boolean b[]=new boolean[v.length];
        for (int i = 0; i < v.length; i++) {
            
            b[i] = v[v.length-i-1].equals("true");
            
        }
    return b;
    }
    
    @Override
    public String toString(){
    String msg="";
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
               msg+=m[i][j]+",";
            }
            msg+="\n";
        }
    return msg;
    }
    
}
