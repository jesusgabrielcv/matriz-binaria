/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Negocio.*;
import javax.swing.JOptionPane;
/**
 *
 * @author gabriel
 */
public class Test {
    public static void main(String[] args) {
        String dato=JOptionPane.showInputDialog("Escribe la matriz: ");
        try {
            MatrizBinaria m=new MatrizBinaria(dato);
            System.out.println("mi matriz es: \n"+m.toString());
            OperacionMatriz myMatriz=(OperacionMatriz)m;
            System.out.println("myMatriz es dispersa: "+myMatriz.isDispersa());
            int vectorDecimal[]=myMatriz.getVectorDecimal();
            for (int x : vectorDecimal) 
                System.out.println(x);
            String dato2=JOptionPane.showInputDialog("Escribe la matriz 2: ");
            MatrizBinaria m2=new MatrizBinaria(dato2);
            MatrizBinaria m3=m.getSumar(m2);
            System.out.println("Mi matriz m3 es: \n"+m3.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
